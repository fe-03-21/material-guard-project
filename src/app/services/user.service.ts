import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private urlUsersAPI = 'https://jsonplaceholder.typicode.com/users/';

  constructor(private http: HttpClient) { }

  getAllUsers() {
    return this.http.get(this.urlUsersAPI);
  }

  getUsers(id: number) {
    return this.http.get(this.urlUsersAPI+id);
  }

  deleteUser(userId: number) {
    return this.http.delete(this.urlUsersAPI+userId);
  }

  updateUser(user: any) {
    return this.http.put(this.urlUsersAPI+user.id, user);
  }

  createUser(user: any) {
    return this.http.post(this.urlUsersAPI, user);
  }
}
