import { Router } from '@angular/router';
import { RouteGuardService } from './../services/route-guard.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit{

  mylogin!: boolean;

  constructor(
      private routeGuardService: RouteGuardService,
      private router: Router  
    ) { }

  ngOnInit(): void {
    this.mylogin = this.routeGuardService.getLogin();
  }

  logout() {
    this.routeGuardService.logoutApp();
    this.router.navigate(['']);
  }

}
