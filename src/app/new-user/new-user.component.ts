import { UserService } from './../services/user.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.css']
})
export class NewUserComponent implements OnInit {
  
  user: any = {name: '', username: '', email: ''};
  
  constructor(
    private userService: UserService,
    private router: Router,
    ) { }

  ngOnInit(): void {
  }

  createUser() {
    this.userService.createUser(this.user).subscribe(response => {
      alert('Oggetto Aggiunto!!! ' + JSON.stringify(response))
    })
    this.router.navigate(['users']);
  }

}
