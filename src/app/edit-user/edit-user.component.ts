import { UserService } from './../services/user.service';
import { Component, OnInit, ɵɵtrustConstantResourceUrl } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {

  user: any = {name: '', username: '', email: ''};
  constructor(
    private route: ActivatedRoute,
    private userService: UserService,
    private router: Router,
    ) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => 
      this.userService.getUsers(params.id)
        .subscribe(response => this.user = response))
  }

  editUser() {
    this.userService.updateUser(this.user).subscribe(response => {
      alert('Oggetto Modificato!!! ' + JSON.stringify(response))
    })
    this.router.navigate(['users']);
  }

}
