import { UserService } from './../services/user.service';
import { Component, OnInit, ɵɵtrustConstantResourceUrl } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {

  displayedColumns: string[] = ['id', 'name', 'username', 'city', 'email', 'phone', 'action'];
  dataSource!: any;

  constructor(
      private userService: UserService,
      private router: Router
      ) { }

  ngOnInit(): void {
    this.userService.getAllUsers().subscribe(response => this.dataSource = response);
  }

  removeUsers(element: any) {
    this.userService.deleteUser(element.id).subscribe(response => {
      //this.dataSource.splice(this.dataSource.indexOf(element), 1)
      this.dataSource = this.dataSource.filter((user: { id: any; }) => user.id !== element.id)
      }
    );
  }

  selectEditUsers(element: any) {
    this.router.navigate(['users', element.id, 'edit']);
  }

}
